package shadowuni.plugin.b.essentials.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class ChatListener implements Listener {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	@EventHandler (priority = EventPriority.LOWEST)
	public void onChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		
		if(!api.getChatManager().hasDisplayName(p)) return;
		String displayName = api.getLibrary().getVault() == null ? api.getChatManager().getDisplayName(p) : ChatColor.translateAlternateColorCodes('&', api.getLibrary().getVault().getChat().getPlayerPrefix(p)) + ChatColor.WHITE + api.getChatManager().getDisplayName(p) + ChatColor.WHITE;
		e.getPlayer().setDisplayName(displayName);
		
		if(!api.isUseChatFilter()) return;
		e.setMessage(api.getChatManager().replaceChat(e.getMessage()));
	}
	
}
