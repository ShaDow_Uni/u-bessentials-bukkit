package shadowuni.plugin.b.essentials.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class ConnectListener implements Listener {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
		String displayName = api.getSQLManager().getDisplayName(p);
		if(displayName != null) {
			p.setDisplayName(displayName);
			api.getChatManager().setDisplayName(p.getName(), displayName);
		}
		
		if(api.getChannelName() != null || p == null) return;
		api.requestChannelName(p);
	}

}