package shadowuni.plugin.b.essentials.listeners;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class PrefixListener implements Listener {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	private HashMap<String, String> lastPrefix = new HashMap<>();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		Bukkit.getScheduler().runTaskLaterAsynchronously(EssentialsPlugin.getInstance(), () -> {
			String prefix = api.getLibrary().getVault().getChat().getPlayerPrefix(p);
			
			if(prefix == null) return;
			lastPrefix.put(p.getName().toLowerCase(), prefix);
			api.getChatManager().sendPermissionPrefix(p.getName(), prefix);
		}, 10);
	}
	
	@EventHandler
	public void onPermissionPrefixChanged(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		String prefix = api.getLibrary().getVault().getChat().getPlayerPrefix(p);
		
		if(prefix == null || lastPrefix.get(p.getName().toLowerCase()).equals(prefix)) return;
		lastPrefix.put(p.getName().toLowerCase(), prefix);
		api.getChatManager().sendPermissionPrefix(p.getName(), prefix);
	}
	
}