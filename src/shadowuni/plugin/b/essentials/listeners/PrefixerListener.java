package shadowuni.plugin.b.essentials.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.prefixer.events.MainPrefixChangeEvent;

public class PrefixerListener implements Listener {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void onPrefixerPrefixChanged(MainPrefixChangeEvent e) {
		api.getChatManager().sendPrefixerPrefix(e.getPrefixPlayer().getName(), e.getPrefix());
	}
	
}