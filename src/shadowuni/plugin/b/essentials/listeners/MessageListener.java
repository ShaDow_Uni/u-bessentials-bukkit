package shadowuni.plugin.b.essentials.listeners;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class MessageListener implements PluginMessageListener {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if(!channel.equals("UBEssentials")) return;
		
		ByteArrayDataInput in = ByteStreams.newDataInput(message);
		String task = in.readUTF();
		
		if(task.equals("DisplayName")) {
			String name = in.readUTF();
			String displayname = in.readUTF();
			api.getChatManager().setDisplayName(name, displayname);
		} else if(task.equals("ChannelName")) {
			String name = in.readUTF();
			api.setChannelName(name);
		}
	}

}