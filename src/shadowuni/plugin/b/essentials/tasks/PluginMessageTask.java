package shadowuni.plugin.b.essentials.tasks;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import shadowuni.plugin.b.essentials.EssentialsPlugin;

public class PluginMessageTask extends BukkitRunnable {
	
	private final byte[] bytes;
	
	public PluginMessageTask(byte[] bytes) {
		this.bytes = bytes;
	}
	
	public void run() {
		Bukkit.getOnlinePlayers()[0].sendPluginMessage(EssentialsPlugin.getInstance(), "UBEssentials", bytes);
	}

}
