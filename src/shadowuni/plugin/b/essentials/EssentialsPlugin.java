package shadowuni.plugin.b.essentials;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.Getter;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.listeners.ChatListener;
import shadowuni.plugin.b.essentials.listeners.ConnectListener;
import shadowuni.plugin.b.essentials.listeners.MessageListener;
import shadowuni.plugin.b.essentials.listeners.PrefixListener;
import shadowuni.plugin.b.essentials.listeners.PrefixerListener;
import shadowuni.plugin.library.api.object.UPlugin;
import shadowuni.plugin.prefixer.PrefixerAPI;

public class EssentialsPlugin extends UPlugin {
	
	@Getter
	private static EssentialsAPI api = new EssentialsAPI();
	@Getter
	private static EssentialsPlugin instance;
	
	public void onUEnable() {
		instance = this;
		api.init();
		
		registerPrefix(api.getPluginPrefix());
		
		loadConfig();
		
		if(!api.getSQLManager().connect()) {
			api.getLibrary().log("MySQL에 접속할 수 없어 플러그인이 비활성화됩니다.");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		api.getSQLManager().loadConfig();
		api.getSQLManager().loadWords();
		api.getLibrary().log(api.getChatManager().getBanWords().size() + "개의 금지어를 불러왔습니다!");
		
		registerPlugins();
		registerPlayers();
		
		Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
		if(api.getLibrary().getVault() != null) {
			Bukkit.getPluginManager().registerEvents(new PrefixListener(), this);
		}
		if(api.isUsePrefixer()) {
			Bukkit.getPluginManager().registerEvents(new PrefixerListener(), this);
		}
		Bukkit.getPluginManager().registerEvents(new ConnectListener(), this);
		
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "UBEssentials");
		Bukkit.getMessenger().registerIncomingPluginChannel(this, "UBEssentials", new MessageListener());
	}
	
	public void onUDisable() {
		api.getSQLManager().close();
	}
	
	public void loadConfig() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		api.setSQLAddress(getConfig().getString("MySQL.주소"));
		api.setSQLPort(getConfig().getInt("MySQL.포트"));
		api.setSQLDatabase(getConfig().getString("MySQL.데이터베이스"));
		api.setSQLUser(getConfig().getString("MySQL.유저"));
		api.setSQLPassword(getConfig().getString("MySQL.비밀번호"));
		api.setSQLTablePrefix(getConfig().getString("MySQL.접두사"));
		api.getLibrary().log("설정을 불러왔습니다!");
	}
	
	private void registerPlugins() {
		if(Bukkit.getPluginManager().getPlugin("U-Prefixer") != null) {
			api.setUsePrefixer(true);
			api.setPrefixerAPI(new PrefixerAPI());
			api.getLibrary().log("U-Prefixer 플러그인과 연동되었습니다.");
		}
	}
	
	private void registerPlayers() {
		for(Player ap : Bukkit.getOnlinePlayers()) {
			String displayName = api.getSQLManager().getDisplayName(ap);
			if(displayName == null) continue;
			api.getChatManager().setDisplayName(ap.getName(), displayName);
		}
	}
	
}
