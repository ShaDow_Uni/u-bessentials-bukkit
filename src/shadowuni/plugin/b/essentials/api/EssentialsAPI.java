package shadowuni.plugin.b.essentials.api;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.managers.ChatManager;
import shadowuni.plugin.b.essentials.api.managers.SQLManager;
import shadowuni.plugin.b.essentials.tasks.PluginMessageTask;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.LibraryAPI;
import shadowuni.plugin.prefixer.PrefixerAPI;

@Getter
public class EssentialsAPI {
	
	private final String pluginPrefix = ChatColor.GRAY + "[ U-BEssentials ] " + ChatColor.WHITE;
	
	@Setter
	private String channelName;
	@Setter
	private String SQLAddress, SQLDatabase, SQLUser, SQLPassword, SQLTablePrefix;
	@Setter
	private int SQLPort;
	
	@Setter
	private boolean usePrefixer, useChatFilter, useLobby;
	
	private String[] filterwords;
	
	private ChatManager chatManager;
	private SQLManager SQLManager;
	
	private LibraryAPI library = LibraryPlugin.getApi();
	@Setter
	private PrefixerAPI prefixerAPI;
	
	public void init() {
		chatManager = new ChatManager();
		SQLManager = new SQLManager();
	}
	
	public void sendLobby(Player p) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		try {
			out.writeUTF("SendLobby");
			out.writeUTF(p.getName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		new PluginMessageTask(out.toByteArray()).runTaskAsynchronously(EssentialsPlugin.getInstance());
	}
	
	public void requestChannelName(Player p) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		try {
			out.writeUTF("GetChannelName");
			out.writeUTF(p.getName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		new PluginMessageTask(out.toByteArray()).runTaskAsynchronously(EssentialsPlugin.getInstance());
	}
	
}