package shadowuni.plugin.b.essentials.api.managers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.entity.Player;

import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class SQLManager {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	private Connection conn = null;
	
	public boolean connect() {
		try {
			conn = DriverManager.getConnection("jdbc:mysql://" + api.getSQLAddress() + ":" + api.getSQLPort() + "/" + api.getSQLDatabase() + "?autoReconnect=true", api.getSQLUser(), api.getSQLPassword());
			api.getLibrary().log("MySQL에 접속되었습니다.");
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			api.getLibrary().log("MySQL에 연결할 수 없습니다!");
			return false;
		}
	}
	
	public void close() {
		try {
			if(conn == null) return;
			conn.close();
			api.getLibrary().log("MySQL과의 연결을 종료했습니다.");
		} catch(Exception e) {
			api.getLibrary().log("MySQL과의 연결을 종료하는 중 오류가 발생했습니다!");
		}
	}
	
	public void loadWords() {
		try {
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "BanWord");
			ResultSet result = state.executeQuery();
			while(result.next()) {
				api.getChatManager().getBanWords().add(result.getString("word"));
			}
			result.close();
			state.close();
		} catch (SQLException e) {}
	}
	
	public String getDisplayName(Player p) {
		return getDisplayName(p.getName());
	}
	
	public String getDisplayName(String name) {
		String temp = null;
		try {
			PreparedStatement state = conn.prepareStatement("select displayName from " + api.getSQLTablePrefix() + "DisplayName where name='" + name +"'");
			ResultSet result = state.executeQuery();
			if(result.next()) {
				temp = result.getString("displayName");
			}
			result.close();
			state.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return temp;
	}
	
	public void loadConfig() {
		api.setUseChatFilter(getConfigInt("UseChatFilter") == 1);
		api.setUseLobby(getConfigInt("UseLobby") == 1);
		loadWords();
	}
	
	public int getConfigInt(String name) {
		try {
			PreparedStatement state = conn.prepareStatement("select value from " + api.getSQLTablePrefix() + "Config where name='" + name + "'");
			ResultSet result = state.executeQuery();
			int i = 0;
			if(result.next()) {
				i = result.getInt("value");
			}
			result.close();
			state.close();
			return i;
		} catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public boolean existsConfig(String name) {
		try {
			PreparedStatement state = conn.prepareStatement("select name from " + api.getSQLTablePrefix() + "Config where name='" + name + "'");
			ResultSet result = state.executeQuery();
			boolean exist = result.next();
			result.close();
			state.close();
			return exist;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
}
