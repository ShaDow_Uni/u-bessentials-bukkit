package shadowuni.plugin.b.essentials.api.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.tasks.PluginMessageTask;

@Setter
@Getter
public class ChatManager {
	
	private List<String> banWords = new ArrayList<>();
	private HashMap<String, String> displayNames = new HashMap<>();
	
	public boolean hasDisplayName(Player p) {
		return hasDisplayName(p.getName());
	}
	
	public boolean hasDisplayName(String name) {
		return displayNames.containsKey(name.toLowerCase());
	}
	
	public void setDisplayName(String name, String displayName) {
		displayNames.put(name.toLowerCase(), displayName);
	}
	
	public String getDisplayName(Player p) {
		return getDisplayName(p.getName());
	}
	
	public String getDisplayName(String name) {
		return displayNames.get(name.toLowerCase());
	}
	
	public void sendDisplayName(String player, String displayName) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		try {
			out.writeUTF("SetDisplayName");
			out.writeUTF(player);
			out.writeUTF(displayName);
		} catch(Exception e) {
			e.printStackTrace();
		}
		new PluginMessageTask(out.toByteArray()).runTaskAsynchronously(EssentialsPlugin.getInstance());
	}
	
	public void sendPrefixerPrefix(String player, String prefix) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		try {
			out.writeUTF("SetPrefixerPrefix");
			out.writeUTF(player);
			out.writeUTF(ChatColor.translateAlternateColorCodes('&', prefix));
		} catch(Exception e) {
			e.printStackTrace();
		}
		new PluginMessageTask(out.toByteArray()).runTaskAsynchronously(EssentialsPlugin.getInstance());
	}
	
	public void sendPermissionPrefix(String player, String prefix) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		try {
			out.writeUTF("SetPermissionPrefix");
			out.writeUTF(player);
			out.writeUTF(ChatColor.translateAlternateColorCodes('&', prefix));
		} catch(Exception e) {
			e.printStackTrace();
		}
		new PluginMessageTask(out.toByteArray()).runTaskAsynchronously(EssentialsPlugin.getInstance());
	}
	
	public String replaceChat(String message) {
		String[] chars = new String[message.length()];
		for (int i = 0; i < chars.length; i++) {
			chars[i] = String.valueOf(message.charAt(i));
		}
		message = message.toLowerCase();
		for(Player player : Bukkit.getOnlinePlayers()) {
			message = message.replace(player.getName().toLowerCase(), replaceWordTo(player.getName(), "-"));
		}
		for(String word : banWords) {
			List<Integer> rWord = replaceWord(message, word);
			for(int i : rWord) {
				chars[i] = "*";
			}
		}
		StringBuilder sb = new StringBuilder();
		for(String c : chars) {
			sb.append(c);
		}
		return sb.toString();
	}
	
	public List<Integer> replaceWord(String message, String word) {
		String p = "[^a-zA-Zㄱ-ㅎ가-힣]";
		if(Pattern.matches("[a-zA-Z]*", word)) {
			p = "[^a-zA-Z]";
		} else if(Pattern.matches("[가-힣]*", word)) {
			p = "[^가-힣]";
		} else if(Pattern.matches("[ㄱ-ㅎ]*", word)) {
			p = "[^ㄱ-ㅎ]";
		}
		String[] chars = new String[message.length()];
		int[] num = new int[chars.length];
		int j = 0;
		for (int i = 0; i < chars.length; i++) {
			chars[i] = String.valueOf(message.charAt(i));
			if(Pattern.matches(p, String.valueOf(chars[i]))) continue;
			num[j++] = i;
		}
		String temp = message.replaceAll(p, "").replace(word, replaceWordTo(word, "*"));
		List<Integer> r = new ArrayList<>();
		for (int i = 0; i < temp.length(); i++) {
			if(!String.valueOf(temp.charAt(i)).equals("*")) continue;
			r.add(num[i]);
		}
		return r;
	}
	
	public String replaceWordTo(String word, String c) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < word.length(); i++) {
			sb.append(c);
		}
		return sb.toString();
	}
	
}
